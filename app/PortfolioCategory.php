<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PortfolioCategory extends Model
{
    // protected $table = 'portfolio_categories';
    protected $fillable = ['name', 'slug'];

    public function portfolio() 
    {
        return $this->HasMany(portfolio::class);
    }
}
