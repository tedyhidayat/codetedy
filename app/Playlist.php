<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    protected $table = 'playlists';

    protected $fillable = ['course_id', 'is_active', 'user_id', 'publish_status', 'status_access', 'name', 'slug', 'level', 'thumbnail', 'description', 'certificate'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function coursetags()
    {
        return $this->belongsToMany(CourseTag::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }
}
