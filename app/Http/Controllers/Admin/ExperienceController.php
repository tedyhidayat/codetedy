<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Experience;
use App\Http\Requests\ExperienceRequest;
use Illuminate\Support\Str;

class ExperienceController extends Controller
{
    public function index()
    {
        $experiences = Experience::latest()->get();
        return view('admin.resume.experience.index', compact('experiences'));
    }
    
    public function create()
    {
        return view('admin.resume.experience.create', [
            'submit' => 'Save',
            'experience' => new Experience(),
        ]);
    }

    public function store(ExperienceRequest $request)
    {
        $attr = $request->all();

        Experience::create($attr);

        session()->flash('success', 'Experience has been added!');

        return redirect()->route('experience-show');
    }

    public function edit($id)
    {
        $experience = Experience::findOrFail($id);
        return view('admin.resume.experience.edit', compact('experience'));
    }

    public function update($id, ExperienceRequest $request)
    {
        $experience = Experience::findOrFail($id);
        $experience->update($request->all());

        session()->flash('success', 'Experience was updated!');

        return redirect()->route('experience-show');
    }

    public function destroy(Experience $experience)
    {
        $experience->delete();

        session()->flash('error', 'Experience was deleted!');

        return redirect()->route('experience-show');
    }
}
