<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Certificate;
use App\Http\Requests\CertificateRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CertificateController extends Controller
{
    public function index()
    {
        $certificates = Certificate::latest()->get();
        return view('admin.resume.certificates.index', compact('certificates'));
    }
    
    public function create()
    {
        return view('admin.resume.certificates.create', [
            'submit' => 'Save',
            'certificate' => new Certificate(),
        ]);
    }

    public function store(CertificateRequest $request)
    {
        $attr = $request->all();

        $slug = Str::slug($request->certificate_name);

        $file = $request->file('file');

        if($file) {
            $fileUrl = $file->storeAs("files/certificates", "{$slug}.{$file->extension()}");
        } else {
            $fileUrl = null;
        }

        $attr['file'] = $fileUrl;

        Certificate::create($attr);

        session()->flash('success', 'Certificate has been added!');

        return redirect()->route('certificate-show');
    }

    public function edit($id)
    {
        $certificate = Certificate::findOrFail($id);
        return view('admin.resume.certificates.edit', compact('certificate'));
    }

    public function update(Certificate $certificate, CertificateRequest $request)
    {
        
        
        $data = $request->all();

        $file = $request->file('file');
        
        if($file) {
            Storage::delete($certificate->file);
            $slug = Str::slug($request->certificate_name);
            $fileUrl = $file->storeAs("files/certificates","{$slug}.{$file->extension()}");
            $data['file'] = $fileUrl;
        } else {
            $fileUrl = $request->file;
        }


        $certificate->update($data);

        session()->flash('success', 'Certificate was updated!');

        return redirect()->route('certificate-show');
    }

    public function destroy(Certificate $certificate)
    {
        Storage::delete($certificate->file);

        $certificate->delete();

        session()->flash('error', 'Certificate was deleted!');

        return redirect()->route('certificate-show');
    }
}
