<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CourseCategory;
use App\Http\Requests\CourseCategoryRequest;
use Illuminate\Support\Str;

class CourseCategoryController extends Controller
{
    public function index()
    {
        $categories = CourseCategory::get();
        return view('admin.courses.categories.index', compact('categories'));
    }
    
    public function create()
    {
        return view('admin.courses.categories.create', [
            'submit' => 'Save',
            'category' => new CourseCategory(),
        ]);
    }

    public function store(CourseCategoryRequest $request)
    {
        $attr = $request->all();

        $attr['slug'] = Str::slug($request->name);

        CourseCategory::create($attr);

        session()->flash('success', 'Course Category has been created!');

        return redirect()->route('course-category');
    }

    public function edit(CourseCategory $category)
    {
        return view('admin.courses.categories.edit', compact('category'));
    }

    public function update(CourseCategory $category, CourseCategoryRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->name);
        $category->update($attr);

        session()->flash('success', 'Course Category was updated!');

        return redirect()->route('course-category');
    }

    public function destroy(CourseCategory $category)
    {
        $category->delete();

        session()->flash('error', 'Course Category was deleted!');

        return redirect()->route('course-category');
    }
}
