<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Course;
use App\CourseCategory;
use App\Http\Requests\CourseRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::latest()->get();
        return view('admin.courses.index', compact('courses'));
    }
    
    public function create()
    {
        return view('admin.courses.create', [
            'submit' => 'Save',
            'categories' => CourseCategory::get(),
            'course' => new Course(),
        ]);
    }

    public function store(CourseRequest $request)
    {
        $attr = $request->all();

        $slug = Str::slug($request->name);

        $thumbnail = $request->file('thumbnail');
        if($thumbnail) {
            $thumbUrl = $thumbnail->storeAs("images/courses/", "{$slug}.{$thumbnail->extension()}");
        } else {
            $thumbUrl = null;
        }

        $attr['thumbnail'] = $thumbUrl;
        $attr['category_id'] = $request->category;
        $attr['slug'] = $slug;

        Course::create($attr);

        session()->flash('success', 'Course has been created!');

        return redirect()->route('course');
    }

    public function edit(Course $course)
    {
        return view('admin.courses.edit',[
            'course' => $course,
            'categories' => CourseCategory::get(),
        ]);
    }

    public function update(Course $course, CourseRequest $request)
    {
        $attr = $request->all();

        $slug = Str::slug($request->name);
        $thumbnail = $request->file('thumbnail');

        if($thumbnail) {
            Storage::delete($course->thumbnail);
            $thumbUrl = $thumbnail->storeAs("images/courses/", "{$slug}.{$thumbnail->extension()}");
        } else {
            $thumbUrl = $course->thumbnail;
        }

        $attr['thumbnail'] = $thumbUrl;
        $attr['category_id'] = request('category');
        
        $course->update($attr);

        session()->flash('success', 'Course was updated!');

        return redirect()->route('course');
    }

    public function destroy(Course $course)
    {
        Storage::delete($course->thumbnail);
        
        $course->delete();

        session()->flash('error', 'Course was deleted!');
        
        return redirect()->route('course');
    }

}
