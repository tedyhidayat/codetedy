<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Video;
use App\Http\Requests\VideoRequest;
use App\Playlist;
use Illuminate\Support\Str;

class VideoController extends Controller
{
    public function index()
    {
        $videos = Video::latest()->get();
        return view('admin.courses.videos.index', compact('videos'));
    }
    
    public function create()
    {
        return view('admin.courses.videos.create', [
            'submit' => 'Save',
            'playlists' => Playlist::get(),
            'video' => new Video(),
        ]);
    }

    public function store(VideoRequest $request)
    {
        $attr = $request->all();

        $slug = Str::slug($request->title);
        // dd($attr);

        $attr['is_active'] = 0;
        $attr['slug'] = $slug;
        $attr['playlist_id'] = request('playlist');

        Video::create($attr);

        session()->flash('success', 'Video has been added!');

        return redirect()->route('video');
    }

    public function edit($id)
    {
        $video = Video::findOrFail($id);
        $playlists = Playlist::get();
        return view('admin.courses.videos.edit', compact('video', 'playlists'));
    }

    public function update($id, VideoRequest $request)
    {
        $attr = $request->all();
        $video = Video::findOrFail($id);
        $attr['playlist_id'] = request('playlist');

        $video->update($attr);

        session()->flash('success', 'Video was updated!');

        return redirect()->route('video');
    }

    public function destroy(Video $video)
    {
        $video->delete();

        session()->flash('error', 'Video was deleted!');

        return redirect()->route('video');
    }

    public function isActive($id, $status) {
        if($status == 1) {
            $attr = 0;
        } elseif($status == 0) {
            $attr = 1;
        }
        
        Video::where('id', $id)->update(['is_active' => $attr]);
        
        session()->flash('success', 'Post status was updated!');

        return redirect()->route('video');
    }
}
