<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Education;
use App\Http\Requests\EducationRequest;
use Illuminate\Support\Str;

class EducationController extends Controller
{
    public function index()
    {
        $educations = Education::latest()->get();
        return view('admin.resume.educations.index', compact('educations'));
    }
    
    public function create()
    {
        return view('admin.resume.educations.create', [
            'submit' => 'Save',
            'education' => new Education(),
        ]);
    }

    public function store(EducationRequest $request)
    {
        $attr = $request->all();

        Education::create($attr);

        session()->flash('success', 'Education has been added!');

        return redirect()->route('education-show');
    }

    public function edit($id)
    {
        $education = Education::findOrFail($id);
        return view('admin.resume.educations.edit', compact('education'));
    }

    public function update($id, EducationRequest $request)
    {
        $education = Education::findOrFail($id);
        $education->update($request->all());

        session()->flash('success', 'Education was updated!');

        return redirect()->route('education-show');
    }

    public function destroy(Education $education)
    {
        $education->delete();

        session()->flash('error', 'Education was deleted!');

        return redirect()->route('education-show');
    }
}
