<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Course;
use App\Playlist;
use App\Coursetag;
use App\Http\Requests\PlaylistRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PlaylistController extends Controller
{
    public function index()
    {
        $playlists = Playlist::latest()->get();
        return view('admin.courses.playlists.index', compact('playlists'));
    }
    
    public function create()
    {
        return view('admin.courses.playlists.create', [
            'submit' => 'Save',
            'courses' => Course::get(),
            'tags' => Coursetag::get(),
            'playlist' => new Playlist(),
        ]);
    }

    public function store(PlaylistRequest $request)
    {
        $attr = $request->all();

        
        $slug = Str::slug($request->name);
        
        $thumbnail = $request->file('thumbnail');
        if($thumbnail) {
            $thumbUrl = $thumbnail->storeAs("images/playlists/", "{$slug}.{$thumbnail->extension()}");
        } else {
            $thumbUrl = null;
        }
        
        $attr['is_active'] = 0;
        $attr['thumbnail'] = $thumbUrl;
        $attr['course_id'] = $request->course;
        $attr['slug'] = $slug;

        Playlist::create($attr)->coursetags()->attach($request->tags);

        session()->flash('success', 'Playlist has been created!');

        return redirect()->route('playlist');
    }

    public function edit(Playlist $playlist)
    {
        return view('admin.courses.playlists.edit',[
            'playlist' => $playlist,
            'tags' => Coursetag::get(),
            'courses' => Course::get(),
        ]);
    }

    public function update(Playlist $playlist, PlaylistRequest $request)
    {
        $attr = $request->all();

        // dd($attr);

        $slug = Str::slug($request->name);
        $thumbnail = $request->file('thumbnail');

        if($thumbnail) {
            Storage::delete($playlist->thumbnail);
            $thumbUrl = $thumbnail->storeAs("images/playlists/", "{$slug}.{$thumbnail->extension()}");
        } else {
            $thumbUrl = $playlist->thumbnail;
        }

        $attr['thumbnail'] = $thumbUrl;
        $attr['course_id'] = request('course');        
        $attr['slug'] = $slug;
        
        $playlist->update($attr);

        $playlist->coursetags()->sync($request->tags);

        session()->flash('success', 'Playlist was updated!');

        return redirect()->route('playlist');
    }

    public function destroy(Playlist $playlist)
    {
        Storage::delete($playlist->thumbnail);
        $playlist->coursetags()->detach();
        
        $playlist->delete();

        session()->flash('error', 'Playlist was deleted!');
        
        return redirect()->route('playlist');
    }
    
    public function isActive($id, $status) {
        if($status == 1) {
            $attr = 0;
        } elseif($status == 0) {
            $attr = 1;
        }
        
        Playlist::where('id', $id)->update(['is_active' => $attr]);
        
        session()->flash('success', 'Playlist status was updated!');

        return redirect()->route('playlist');
    }
}
