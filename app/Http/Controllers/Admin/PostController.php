<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use App\PostCategory;
use App\Tag;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->get();
        return view('admin.posts.index', compact('posts'));
    }
    
    public function create()
    {
        return view('admin.posts.create', [
            'submit' => 'Save',
            'tags' => Tag::get(),
            'categories' => PostCategory::get(),
            'post' => new Post(),
        ]);
    }

    public function store(PostRequest $request)
    {
        $attr = $request->all();

        $slug = Str::slug($request->title);

        $thumbnail = $request->file('thumbnail');
        if($thumbnail) {
            $thumbUrl = $thumbnail->storeAs("images/posts/", "{$slug}.{$thumbnail->extension()}");
        } else {
            $thumbUrl = null;
        }

        $attr['thumbnail'] = $thumbUrl;
        $attr['category_id'] = $request->category;
        $attr['slug'] = $slug;

        Post::create($attr)->tags()->attach($request->tags);

        session()->flash('success', 'Post has been created!');

        return redirect()->route('post-show');
    }

    public function edit(Post $post)
    {
        return view('admin.posts.edit',[
            'post' => $post,
            'tags' => Tag::get(),
            'categories' => PostCategory::get(),
        ]);
    }

    public function update(Post $post, PostRequest $request)
    {
        $attr = $request->all();

        $slug = Str::slug($request->title);
        $thumbnail = $request->file('thumbnail');

        if($thumbnail) {
            Storage::delete($post->thumbnail);
            $thumbUrl = $thumbnail->storeAs("images/posts/", "{$slug}.{$thumbnail->extension()}");
        } else {
            $thumbUrl = $post->thumbnail;
        }

        $attr['thumbnail'] = $thumbUrl;
        $attr['category_id'] = request('category');        
        $attr['slug'] = $slug;
        
        $post->update($attr);

        $post->tags()->sync($request->tags);

        session()->flash('success', 'Post was updated!');

        return redirect()->route('post-show');
    }

    public function destroy(Post $post)
    {
        Storage::delete($post->thumbnail);
        $post->tags()->detach();
        
        $post->delete();

        session()->flash('error', 'Post was deleted!');
        
        return redirect()->route('post-show');
    }
    
    public function isActive($id, $status) {
        if($status == 1) {
            $attr = 0;
        } elseif($status == 0) {
            $attr = 1;
        }
        
        Post::where('id', $id)->update(['is_active' => $attr]);
        
        session()->flash('success', 'Post status was updated!');

        return redirect()->route('post-show');
    }
}
