<?php

namespace App\Http\Controllers\Admin;

use App\Coursetag;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseTagRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CourseTagController extends Controller
{
    public function index()
    {
        $tags = Coursetag::get();
        return view('admin.courses.tags.index', compact('tags'));
    }
    
    public function create()
    {
        return view('admin.courses.tags.create', [
            'submit' => 'Save',
            'tag' => new Coursetag(),
        ]);
    }

    public function store(CourseTagRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->name);

        Coursetag::create($attr);

        session()->flash('success', 'Course Tag has been created!');

        return redirect()->route('course-tag');
    }

    public function edit(Coursetag $tag)
    {
        return view('admin.courses.tags.edit', compact('tag'));
    }

    public function update(Coursetag $Tag, CourseTagRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->name);
        $Tag->update($attr);

        session()->flash('success', 'Course Tag was updated!');

        return redirect()->route('course-tag');
    }

    public function destroy(Coursetag $Tag)
    {
        $Tag->delete();

        session()->flash('error', 'Course Tag was deleted!');

        return redirect()->route('course-tag');
    }
}
