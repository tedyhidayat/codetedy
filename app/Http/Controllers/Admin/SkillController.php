<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Skill;
use App\Http\Requests\SkillRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SkillController extends Controller
{
    public function index()
    {
        $skills = Skill::latest()->get();
        return view('admin.resume.skills.index', compact('skills'));
    }
    
    public function create()
    {
        return view('admin.resume.skills.create', [
            'submit' => 'Save',
            'skill' => new Skill(),
        ]);
    }

    public function store(SkillRequest $request)
    {
        $attr = $request->all();

        $slug = Str::slug($request->name);

        $file = $request->file('thumbnail');

        if($file) {
            $fileUrl = $file->storeAs("images/", "{$slug}.{$file->extension()}");
        } else {
            $fileUrl = null;
        }

        $attr['thumbnail'] = $fileUrl;

        Skill::create($attr);

        session()->flash('success', 'Skill has been added!');

        return redirect()->route('skill-show');
    }

    public function edit($id)
    {
        $skill = Skill::findOrFail($id);
        return view('admin.resume.skills.edit', compact('skill'));
    }

    public function update(Skill $skill, SkillRequest $request)
    {
        
        
        $data = $request->all();

        $file = $request->file('thumbnail');
        
        if($file) {
            Storage::delete($skill->thumbnail);
            $slug = Str::slug($request->name);
            $fileUrl = $file->storeAs("images/","{$slug}.{$file->extension()}");
            $data['thumbnail'] = $fileUrl;
        } else {
            $fileUrl = $request->file;
        }


        $skill->update($data);

        session()->flash('success', 'Skill was updated!');

        return redirect()->route('skill-show');
    }

    public function destroy(Skill $skill)
    {
        Storage::delete($skill->thumbnail);

        $skill->delete();

        session()->flash('error', 'Skill was deleted!');

        return redirect()->route('skill-show');
    }
}
