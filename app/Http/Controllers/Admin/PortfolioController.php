<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Portfolio;
use App\PortfolioCategory;
use App\PortfolioGallery;
use App\Http\Requests\PortfolioRequest;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PortfolioController extends Controller
{
    public function index()
    {
        $portfolios = Portfolio::latest()->get();
        return view('admin.portfolio.index', compact('portfolios'));
    }
    
    public function create()
    {
        return view('admin.portfolio.create', [
            'submit' => 'Save',
            'categories' => PortfolioCategory::get(),
            'portfolio' => new Portfolio(),
        ]);
    }

    public function store(PortfolioRequest $request)
    {
        $attr = $request->all();
        // dd($attr);

        $slug = Str::slug($request->name);

        $portfolio = new Portfolio;
        $portfolio->category_id = $attr['category'];
        $portfolio->name = $attr['name'];
        $portfolio->slug = $slug;
        $portfolio->client = $attr['client'];
        $portfolio->link = $attr['link'];
        $portfolio->notes = $attr['notes'];
        $portfolio->date_created = $attr['created_date'];
        $portfolio->save();

        if(!empty($attr['thumbnail'])) {
            foreach($attr['thumbnail'] as $img) {
                $slugImg = $slug . '-' . rand();
                $imgname = $img->storeAs("images/portfolio/", "{$slugImg}.{$img->extension()}");
                PortfolioGallery::create([
                    'portfolio_id' => $portfolio->id,
                    'img_name' => $imgname,
                ]);
            }
        }

        session()->flash('success', 'Portfolio has been created!');

        return redirect()->route('portfolio-show');
    }

    public function edit(Portfolio $Portfolio)
    {
        return view('admin.portfolio.edit',[
            'portfolio' => $Portfolio,
            'categories' => PortfolioCategory::get(),
            
        ]);
    }

    public function update(Portfolio $portfolio, PortfolioRequest $request)
    {
        $attr = $request->all();

        $attr['category_id'] = request('category');
        $attr['date_created'] = request('created_date');
        
        $portfolio->update($attr);

        session()->flash('success', 'Portfolio was updated!');

        return redirect()->route('portfolio-show');
        // return redirect()->back();
    }

    public function destroy(Portfolio $portfolio)
    {
        $images = PortfolioGallery::where('portfolio_id', $portfolio->id)->get();

        if(!empty($images)) {
            foreach($images as $image) {
                Storage::delete($image->img_name);
            }
        }
        
        $portfolio->delete();

        session()->flash('error', 'Portfolio was deleted!');
        
        return redirect()->route('portfolio-show');
    }

    public function isActive($id, $status) {
        if($status == 1) {
            $attr = 0;
        } elseif($status == 0) {
            $attr = 1;
        }
        
        Portfolio::where('id', $id)->update(['is_active' => $attr]);
        
        session()->flash('success', 'Portfolio status was updated!');

        return redirect()->route('portfolio-show');
    }

    public function imgDelete($id)
    {

        $image = PortfolioGallery::findOrFail($id);

        Storage::delete($image->img_name);
        
        $image->delete();

        session()->flash('error', 'Portfolio was deleted!');
        
        return redirect()->back();
    }

    public function imgAdd()
    {
        $attr = request()->all();

        // dd($attr);

        $slug = Str::slug(request('name'));

        if(!empty($attr['thumbnail'])) {
            foreach($attr['thumbnail'] as $img) {
                $slugImg = $slug . '-' . rand();
                $imgname = $img->storeAs("images/portfolio/", "{$slugImg}.{$img->extension()}");
                PortfolioGallery::create([
                    'portfolio_id' => request('portfolio_id'),
                    'img_name' => $imgname,
                ]);
            }
        }

        session()->flash('success', 'Image has been added!');

        return redirect()->back();
    }
}
