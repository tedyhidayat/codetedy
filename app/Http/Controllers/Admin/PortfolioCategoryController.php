<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PortfolioCategory;
use App\Http\Requests\PortfolioCategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PortfolioCategoryController extends Controller
{
    public function index()
    {
        $categories = PortfolioCategory::get();
        return view('admin.portfolio.categories.index', compact('categories'));
    }
    
    public function create()
    {
        return view('admin.portfolio.categories.create', [
            'submit' => 'Save',
            'category' => new PortfolioCategory(),
        ]);
    }

    public function store(PortfolioCategoryRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->name);

        PortfolioCategory::create($attr);

        session()->flash('success', 'Portfolio Category has been created!');

        return redirect()->route('portfolio-category');
    }

    public function edit(PortfolioCategory $category)
    {
        return view('admin.portfolio.categories.edit', compact('category'));
    }

    public function update(PortfolioCategory $category, PortfolioCategoryRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->name);
        $category->update($attr);

        session()->flash('success', 'Portfolio Category was updated!');

        return redirect()->route('portfolio-category');
    }

    public function destroy(PortfolioCategory $category)
    {
        $category->delete();

        session()->flash('error', 'Portfolio Category was deleted!');

        return redirect()->route('portfolio-category');
    }
}
