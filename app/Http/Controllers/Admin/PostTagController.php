<?php

namespace App\Http\Controllers\Admin;

use App\Tag;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostTagRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostTagController extends Controller
{
    public function index()
    {
        $tags = Tag::get();
        return view('admin.posts.tags.index', compact('tags'));
    }
    
    public function create()
    {
        return view('admin.posts.tags.create', [
            'submit' => 'Save',
            'tag' => new Tag(),
        ]);
    }

    public function store(PostTagRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->name);

        Tag::create($attr);

        session()->flash('success', 'Post Tag has been created!');

        return redirect()->route('post-tag');
    }

    public function edit(Tag $tag)
    {
        return view('admin.posts.tags.edit', compact('tag'));
    }

    public function update(Tag $Tag, PostTagRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->name);
        $Tag->update($attr);

        session()->flash('success', 'Post Tag was updated!');

        return redirect()->route('post-tag');
    }

    public function destroy(Tag $Tag)
    {
        $Tag->delete();

        session()->flash('error', 'Post Tag was deleted!');

        return redirect()->route('post-tag');
    }
}
