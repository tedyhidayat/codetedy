<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PostCategory;
use App\Http\Requests\PostCategoryRequest;
use Illuminate\Support\Str;

class PostCategoryController extends Controller
{
    public function index()
    {
        $categories = PostCategory::get();
        return view('admin.posts.categories.index', compact('categories'));
    }
    
    public function create()
    {
        return view('admin.posts.categories.create', [
            'submit' => 'Save',
            'category' => new PostCategory(),
        ]);
    }

    public function store(PostCategoryRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->title);

        PostCategory::create($attr);

        session()->flash('success', 'Post Category has been created!');

        return redirect()->route('post-category');
    }

    public function edit(PostCategory $category)
    {
        return view('admin.posts.categories.edit', compact('category'));
    }

    public function update(PostCategory $category, PostCategoryRequest $request)
    {
        $attr = $request->all();
        $attr['slug'] = Str::slug($request->title);
        $category->update($attr);

        session()->flash('success', 'Post Category was updated!');

        return redirect()->route('post-category');
    }

    public function destroy(PostCategory $category)
    {
        $category->delete();

        session()->flash('error', 'Post Category was deleted!');

        return redirect()->route('post-category');
    }
}
