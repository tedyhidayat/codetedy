<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'education_name' => 'required|min:5',
            'ijazah_number' => 'required|min:5',
            'graduation' => 'required|min:4|max:4',
            'score' => 'required',
        ];
    }
}
