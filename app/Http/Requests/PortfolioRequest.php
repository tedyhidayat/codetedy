<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PortfolioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'date_created' => 'date',
            'client' => 'min:5',
            'link' => 'min:5',
            'thumbnail[]' => 'image|mimes:jpg,jpeg,png,svg|max:5072',
        ];
    }
}
