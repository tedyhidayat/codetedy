<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CertificateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'certificate_name' => 'required|min:5',
            'organizer' => 'required|min:5',
            'place' => 'min:5',
            'file' => 'file|mimes:pdf,doc,docx,jpg,jpeg,png|max:2048'
        ];
    }
}
