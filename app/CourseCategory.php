<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    public $timestamps = false;
    protected $table = 'course_categories';
    protected $fillable = ['name', 'slug'];

    public function course() {
        return $this->hasMany(Course::class);
    }
}
