<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder\Class_;

class PortfolioGallery extends Model
{
    protected $table = 'portfolio_gallery';

    protected $fillable = ['img_name','portfolio_id'];

    public function portfolio() 
    {
        return $this->BelongsTo(PortfolioCategory::class);
    }
}
