<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name', 'slug', 'thumbnail', 'notes', 'category_id'];

    public function course_category()
    {
        return $this->belongsTo(CourseCategory::class, 'category_id');
    }

    public function playlist()
    {
        return $this->hasMany(Playlist::class);
    }
}
