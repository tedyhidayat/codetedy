<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder\Class_;

class Post extends Model
{
    protected $fillable = ['title', 'slug', 'category_id', 'is_active', 'user_id', 'body', 'thumbnail'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    
    public function category()
    {
        return $this->belongsTo(PostCategory::class);
    }
}
