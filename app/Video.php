<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';

    protected $fillable = ['playlist_id', 'is_active', 'user_id', 'status_access', 'title', 'slug', 'url_video', 'notes'];

    public function playlist()
    {
        return $this->belongsTo(Playlist::class);
    }
}
