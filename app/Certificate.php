<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    // protected $table = 'certificate';
    protected $fillable = ['certificate_name', 'certificate_number', 'organizer', 'place', 'notes', 'file'];
}
