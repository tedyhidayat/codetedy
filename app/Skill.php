<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    // protected $table = 'tbl_skills';
    protected $fillable = ['name', 'level', 'thumbnail', 'notes'];
}
