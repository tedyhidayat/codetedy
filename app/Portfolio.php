<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use PhpParser\Builder\Class_;

class Portfolio extends Model
{
    protected $dates = ['date_created'];
    protected $table = 'portfolio';

    protected $fillable = ['name','slug','category_id','client','link','notes','date_created'];

    public function category() 
    {
        return $this->BelongsTo(PortfolioCategory::class);
    }
    
    public function image() 
    {
        return $this->hasMany(PortfolioGallery::class);
    }
}
