<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coursetag extends Model
{
    protected $fillable = ['name', 'slug'];
    protected $table = 'coursetags';

    public function playlists() {
        return $this->belongsToMany(Playlist::class);
    }
}
