@include('layouts.admin.header')
@include('layouts.admin.topbar')
@include('layouts.admin.sidebar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    @include('admin.alerts')

    @yield('content')

</div>
<!-- /.content-wrapper -->
@include('layouts.admin.footer')