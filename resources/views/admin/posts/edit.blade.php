@extends('layouts.admin.app', ['title' => 'Edit Post'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('post-update', $post->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('patch')
                                @include('admin.posts.form')
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection