<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $post->title }}" autofocus>
            @error('title')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Category</label>
            <select name="category" class="select2" style="width: 100%;">
                @if(!$post->category_id)<option selected disabled>Select Catergory</option> @endif
                @foreach ($categories as $category)
                    <option {{ $post->category_id === $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->title }}</option>
                @endforeach
            </select>
            @error('category')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Tags</label>
            <select name="tags[]" class="form-control select2" data-placeholder="Select tags" style="width: 100%;" multiple>
                @foreach ($post->tags as $tag)
                    <option selected value="{{ $tag->id }}">{{ $tag->name }}</option>
                @endforeach
                @foreach ($tags as $tag)
                    <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                @endforeach
            </select>
            @error('tags')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label>Post Thumbnail</label>
            <input type="file" name="thumbnail" class="dropify"
            @if($post->thumbnail ) data-default-file="{{ asset("storage/" . $post->thumbnail) }}" @endif
            data-max-file-size="3M"
            data-errors-position="outside"
            data-allowed-file-extensions="jpeg png jpg svg"
            />
            @error('thumbnail')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Post Content</label>
            <textarea name="body" cols="30" rows="10" class="textarea">{{ old('body') ?? $post->body }}</textarea>
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>