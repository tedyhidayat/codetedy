@extends('layouts.admin.app', ['title' => 'Create New Post Tag'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-5">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('post-tag-store') }}" method="post">
                            @csrf
                            @include('admin.posts.tags.form')
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection