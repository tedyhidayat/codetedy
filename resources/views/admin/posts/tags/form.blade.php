<div class="form-group">
    <label>Tag</label>
    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ?? $tag->name }}">
    @error('name')
        <div class="text-danger text small invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>