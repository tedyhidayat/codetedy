@extends('layouts.admin.app', ['title' => 'Edit Post Tag'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-5">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('post-tag-update', $tag->id) }}" method="post">
                                @csrf
                                @method('patch')
                                @include('admin.posts.tags.form')
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection