@extends('layouts.admin.app', ['title' => 'Posts'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('post-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Create New Post</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="40">NO.</th>
                                        <th>Thumbnail</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Created</th>
                                        <th>Status</th>
                                        <th width="130">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach ($posts as $post)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td><img src="{{ asset("storage/" . $post->thumbnail) }}" class="img-thumbnail" width="60"></td>
                                            <td>{{ $post->title }}</td>
                                            <td>{{ $post->category->title }}</td>
                                            <td>{{ $post->created_at->format('d F Y') }}</td>
                                            <td>
                                                @if($post->is_active == 1)
                                                    <a href="{{ route('post-isActive', [$post->id, $post->is_active]) }}" class="btn btn-success btn-xs"> ON</a>
                                                @else
                                                    <a href="{{ route('post-isActive', [$post->id, $post->is_active]) }}" class="btn btn-danger btn-xs"> OFF</a>
                                                @endif
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalDetail{{ $post->id }}">
                                                    Detail
                                                </button>
                                                <a href="{{ route('post-edit', $post->id) }}" class="btn btn-info btn-xs">Edit</a>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $post->id }}">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                        <div>
                            <!-- Modal -->
                            @foreach ($posts as $post)
                            <div class="modal fade" id="modal{{ $post->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{ $post->title }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-xs"
                                                data-dismiss="modal">Cancle</button>
                                            <form action="{{ route('post-destroy', $post->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="modalDetail{{ $post->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalCenterTitle">
                                                <a href="{{ route('post-edit', $post->id) }}" class="btn btn-primary btn-xs"><i class="fas fa-edit"></i></a>
                                                &nbsp;
                                                {{ $post->title }}
                                            </h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <img src="{{ asset("storage/" . $post->thumbnail) }}" class="img-fluid">
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table">
                                                        <tr>
                                                            <td>Created</td>
                                                            <td>:</td>
                                                            <td>{{ $post->created_at->format("l, d F Y") }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Category</td>
                                                            <td>:</td>
                                                            <td>{{ $post->category->title }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tags</td>
                                                            <td>:</td>
                                                            <td>
                                                                @foreach ($post->tags as $tag)
                                                                    {{ $tag->name }},
                                                                @endforeach    
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-12 p-3">
                                                    {!! nl2br($post->body) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection