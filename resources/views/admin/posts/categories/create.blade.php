@extends('layouts.admin.app', ['title' => 'Create New Post Categories'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-5">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('post-category-store') }}" method="post">
                            @csrf
                            @include('admin.posts.categories.form')
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection