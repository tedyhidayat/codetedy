<div class="form-group">
    <label>Title Category</label>
<input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $category->title }}">
    @error('title')
        <div class="text-danger text small invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>