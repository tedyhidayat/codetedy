@extends('layouts.admin.app', ['title' => 'Edit Post'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('course-update', $course->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('patch')
                                @include('admin.courses.form')
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection