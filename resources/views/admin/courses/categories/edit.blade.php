@extends('layouts.admin.app', ['title' => 'Edit Post Categories'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-5">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('course-category-update', $category->id) }}" method="post">
                                @csrf
                                @method('patch')
                                @include('admin.courses.categories.form')
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection