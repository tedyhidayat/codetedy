<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Course Name</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ?? $course->name }}" autofocus>
            @error('name')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>Course Category</label>
            <select name="category" class="select2" style="width: 100%;">
                @if(!$course->category_id)<option selected disabled>Select Catergory</option> @endif
                @foreach ($categories as $category)
                    <option {{ $course->category_id === $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            @error('category')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Thumbnail</label>
            <input type="file" name="thumbnail" class="dropify"
            @if($course->thumbnail ) data-default-file="{{ asset("storage/" . $course->thumbnail) }}" @endif
            data-max-file-size="2M"
            data-allowed-file-extensions="jpeg png jpg svg"
            />
            @error('thumbnail')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <label>Notes</label>
            <textarea name="notes" cols="30" rows="10" class="textarea">{{ old('notes') ?? $course->notes }}</textarea>
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Save Changes' }}</button>
