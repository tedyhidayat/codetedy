<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $video->title }}" autofocus>
            @error('title')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Playlist</label>
            <select name="playlist" class="select2" style="width: 100%;">
                @if(!$video->playlist_id)<option selected disabled>Select Playlist</option> @endif
                @foreach ($playlists as $playlist)
                    <option {{ $video->playlist_id == $playlist->id ? 'selected' : '' }} value="{{ $playlist->id }}">{{ $playlist->name }}</option>
                @endforeach
            </select>
            @error('playlist')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Status Access</label>
            <select name="status_access" class="form-control select2 @error('status_access') is-invalid @enderror" style="width: 100%;">
                @if($video->status_access == 'premium')
                <option selected value="premium">Premium</option>
                <option value="free">Free</option>
                @elseif($video->status_access == 'free')
                <option selected value="free">Free</option>
                <option value="premium">Premium</option>
                @else
                <option selected disabled>Select Status Access</option>
                <option value="free">Free</option>
                <option value="premium">Premium</option>
                @endif
            </select>
            @error('status_access')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Notes</label>
            <textarea name="notes" cols="30" rows="11" class="form-control">{{ old('notes') ?? $video->notes }}</textarea>
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label>Video URL</label>
            <textarea name="url_video" cols="30" rows="10" class="textarea2">{{ old('url_video') ?? $video->url_video }}</textarea>
        </div>
        <div class="form-group videoNew"></div>
    </div>
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Save Changes' }}</button>