@extends('layouts.admin.app', ['title' => 'Edit Video Detail'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('video-update', $video->id) }}" method="post">
                                @csrf
                                @method('patch')
                                @include('admin.courses.videos.form')
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('script-after')

<script type="text/javascript">
    $(function () {
        // Summernote
        $('.textarea2').summernote({
            height: "490px",
            styleWithSpan: false,
            placeholder: 'Click video button to adding your video',
            toolbar: [
                'video','link'
            ],
        })
    })
</script>
@endsection