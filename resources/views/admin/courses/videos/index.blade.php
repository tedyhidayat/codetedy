@extends('layouts.admin.app', ['title' => 'videos'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('video-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Add New Video</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="40">NO.</th>
                                        <th>Playlist</th>
                                        <th>Title</th>
                                        <th>Status Access</th>
                                        <th>Created</th>
                                        <th>URL Video</th>
                                        <th>Is Active</th>
                                        <th width="130">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach ($videos as $video)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $video->playlist->name }}</td>
                                            <td>{{ $video->title }}</td>
                                            <td>{{ $video->status_access }}</td>
                                            <td>{{ $video->created_at->format('d F Y') }}</td>
                                            <td>{{ $video->url_video }}</td>
                                            <td>
                                                @if($video->is_active == 1)
                                                    <a href="{{ route('video-isActive', [$video->id, $video->is_active]) }}" class="btn btn-success btn-xs"> ON</a>
                                                @else
                                                    <a href="{{ route('video-isActive', [$video->id, $video->is_active]) }}" class="btn btn-danger btn-xs"> OFF</a>
                                                @endif
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalDetail{{ $video->id }}">
                                                    Detail
                                                </button>
                                                <a href="{{ route('video-edit', $video->id) }}" class="btn btn-info btn-xs">Edit</a>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $video->id }}">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                        <div>
                            <!-- Modal -->
                            @foreach ($videos as $video)
                            <div class="modal fade" id="modal{{ $video->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{ $video->title }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-xs"
                                                data-dismiss="modal">Cancle</button>
                                            <form action="{{ route('video-destroy', $video->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="modalDetail{{ $video->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalCenterTitle">
                                                <a href="{{ route('video-edit', $video->id) }}" class="btn btn-primary btn-xs"><i class="fas fa-edit"></i></a>
                                                &nbsp;
                                                {{ $video->title }}
                                            </h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col">
                                                    {!! $video->url_video !!}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <table class="table">
                                                        <tr>
                                                            <td>Created</td>
                                                            <td>:</td>
                                                            <td><b>{{ $video->created_at->format("l, d F Y") }}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Playlist</td>
                                                            <td>:</td>
                                                            <td><b>{{ $video->playlist->name }}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status Access</td>
                                                            <td>:</td>
                                                            <td><b>{{ $video->status_access }}</b></td>
                                                        </tr>
                                                    </table>
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 p-3">
                                                    <h4 class="mb-2">Notes</h4>
                                                    {!! nl2br($video->notes) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection