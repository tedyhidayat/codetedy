@extends('layouts.admin.app', ['title' => 'Course'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('course-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Create New Course</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="40">NO.</th>
                                        <th>Thumbnail</th>
                                        <th>Course Name</th>
                                        <th>Category</th>
                                        <th>Created</th>
                                        <th>Notes</th>
                                        <th width="130">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach ($courses as $course)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td><img src="{{ asset("storage/" . $course->thumbnail) }}" class="img-thumbnail" width="60"></td>
                                            <td>{{ $course->name }}</td>
                                            <td>{{ $course->course_category->name }}</td>
                                            <td>{{ $course->created_at->format('d F Y') }}</td>
                                            <td>{{ $course->notes }}</td>
                                            <td>
                                                <a href="{{ route('course-edit', $course->id) }}" class="btn btn-info btn-xs">Edit</a>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $course->id }}">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                        <div>
                            <!-- Modal -->
                            @foreach ($courses as $course)
                            <div class="modal fade" id="modal{{ $course->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{ $course->name }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-xs"
                                                data-dismiss="modal">Cancel</button>
                                            <form action="{{ route('course-destroy', $course->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            @endforeach
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection