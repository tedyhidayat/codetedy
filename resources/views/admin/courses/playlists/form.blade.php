<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            <label>Playlist Name</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ?? $playlist->name }}" autofocus>
            @error('name')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Select Course</label>
            <select name="course" class="select2 @error('course') is-invalid @enderror" style="width: 100%;">
                @if(!$playlist->course_id)<option selected disabled>Select Course</option> @endif
                @foreach ($courses as $course)
                    <option {{ $playlist->course_id == $course->id ? 'selected' : '' }} value="{{ $course->id }}">{{ $course->name }}</option>
                @endforeach
            </select>
            @error('course')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Status Access</label>
            <select name="status_access" class="form-control select2 @error('status_access') is-invalid @enderror" style="width: 100%;">
                @if($playlist->status_access == 'premium')
                <option selected value="premium">Premium</option>
                <option value="free">Free</option>
                @elseif($playlist->status_access == 'free')
                <option selected value="free">Free</option>
                <option value="premium">Premium</option>
                @else
                <option selected disabled>Select Status Access</option>
                <option value="free">Free</option>
                <option value="premium">Premium</option>
                @endif
            </select>
            @error('status_access')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Publish Status</label>
            <select name="publish_status" class="form-control select2 @error('publish_status') is-invalid @enderror" style="width: 100%;">
                @if($playlist->publish_status == 'coming soon')
                    <option selected value="coming soon">Coming Soon</option>
                    <option value="on progress">On Progress</option>
                    <option value="completed">Completed</option>
                @elseif($playlist->publish_status == 'on progress')
                    <option selected value="on progress">On Progress</option>
                    <option value="coming soon">Coming Soon</option>
                    <option value="completed">Completed</option>
                @elseif($playlist->publish_status == 'completed')
                    <option selected value="completed">Completed</option>
                    <option value="coming soon">Coming Soon</option>
                    <option value="on progress">On Progress</option>
                @else
                    <option disabled  selected >Select Publish Status</option>
                    <option value="coming soon">Coming Soon</option>
                    <option value="on progress">On Progress</option>
                    <option value="completed">Completed</option>
                @endif
            </select>
            @error('publish_status')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Level</label>
            <select name="level" class="form-control select2 @error('level') is-invalid @enderror" style="width: 100%;">
                @if($playlist->level == 'pemula')
                    <option selected value="pemula">Pemula</option>
                    <option value="menengah">Menengah</option>
                    <option value="mahir">Mahir</option>
                @elseif($playlist->level == 'menengah')
                    <option selected value="menengah">Menengah</option>
                    <option value="pemula">Pemula</option>
                    <option value="mahir">Mahir</option>
                @elseif($playlist->level == 'mahir')
                    <option selected value="mahir">Mahir</option>
                    <option value="menengah">Menengah</option>
                    <option value="pemula">Pemula</option>
                @else
                    <option selected disabled>Select Level</option>
                    <option value="pemula">Pemula</option>
                    <option value="menengah">Menengah</option>
                    <option value="mahir">Mahir</option>
                @endif
            </select>
            @error('level')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label>Certificate</label>
            <select name="certificate" class="form-control select2 @error('certificate') is-invalid @enderror" style="width: 100%;">
                @if($playlist->certificate == 'yes')
                    <option selected value="yes">Yes</option>
                    <option value="no">No</option>
                @elseif($playlist->certificate == 'no')
                    <option selected value="no">No</option>
                    <option value="yes">Yes</option>
                @else
                    <option selected disabled>Status Certificate</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                @endif
            </select>
            @error('certificate')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Tags</label>
            <select name="tags[]" class="form-control select2" data-placeholder="Select tags" style="width: 100%;" multiple>
                @foreach ($playlist->coursetags as $tag)
                    <option selected value="{{ $tag->id }}">{{ $tag->name }}</option>
                @endforeach
                @foreach ($tags as $tag)
                    <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                @endforeach
            </select>
            @error('tags')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Thumbnail</label>
            <input type="file" name="thumbnail" class="dropify"
            @if($playlist->thumbnail ) data-default-file="{{ asset("storage/" . $playlist->thumbnail) }}" @endif
            data-max-file-size="2M"
            data-errors-position="outside"
            data-allowed-file-extensions="jpeg png jpg svg"
            />
            @error('thumbnail')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" cols="30" rows="10" class="textarea">{{ old('description') ?? $playlist->description }}</textarea>
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>