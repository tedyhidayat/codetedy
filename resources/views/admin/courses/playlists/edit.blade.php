@extends('layouts.admin.app', ['title' => 'Edit Playlist'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('playlist-update', $playlist->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('patch')
                                @include('admin.courses.playlists.form')
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection