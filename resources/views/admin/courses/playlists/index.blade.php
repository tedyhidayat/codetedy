@extends('layouts.admin.app', ['title' => 'Playlists'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('playlist-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Create New playlist</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="40">NO.</th>
                                        <th>Thumbnail</th>
                                        <th>Title</th>
                                        <th>Course</th>
                                        <th>Created</th>
                                        <th>Total Video</th>
                                        <th>Status</th>
                                        <th>Is Active</th>
                                        <th width="130">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach ($playlists as $playlist)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td><img src="{{ asset("storage/" . $playlist->thumbnail) }}" class="img-thumbnail" width="60"></td>
                                            <td>{{ $playlist->name }}</td>
                                            <td>{{ $playlist->course->name }}</td>
                                            <td>{{ $playlist->created_at->format('d F Y') }}</td>
                                            <td>{{ count($playlist->videos) }}</td>
                                            <td>{{ $playlist->publish_status }}</td>
                                            <td>
                                                @if($playlist->is_active == 1)
                                                    <a href="{{ route('playlist-isActive', [$playlist->id, $playlist->is_active]) }}" class="btn btn-success btn-xs"> ON</a>
                                                @else
                                                    <a href="{{ route('playlist-isActive', [$playlist->id, $playlist->is_active]) }}" class="btn btn-danger btn-xs"> OFF</a>
                                                @endif
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalDetail{{ $playlist->id }}">
                                                    Detail
                                                </button>
                                                <a href="{{ route('playlist-edit', $playlist->id) }}" class="btn btn-info btn-xs">Edit</a>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $playlist->id }}">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                        <div>
                            <!-- Modal -->
                            @foreach ($playlists as $playlist)
                            <div class="modal fade" id="modal{{ $playlist->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{ $playlist->name }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-xs"
                                                data-dismiss="modal">Cancle</button>
                                            <form action="{{ route('playlist-destroy', $playlist->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="modalDetail{{ $playlist->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalCenterTitle">
                                                <a href="{{ route('playlist-edit', $playlist->id) }}" class="btn btn-primary btn-xs"><i class="fas fa-edit"></i></a>
                                                &nbsp;
                                                {{ $playlist->name }}
                                            </h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <img src="{{ asset("storage/" . $playlist->thumbnail) }}" class="img-fluid">
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table">
                                                        <tr>
                                                            <td>Is Active</td>
                                                            <td>:</td>
                                                            <td>
                                                                @if($playlist->is_active == 1)
                                                                    <a href="{{ route('playlist-isActive', [$playlist->id, $playlist->is_active]) }}" class="btn btn-success btn-xs"> ON</a>
                                                                @else
                                                                    <a href="{{ route('playlist-isActive', [$playlist->id, $playlist->is_active]) }}" class="btn btn-danger btn-xs"> OFF</a>
                                                                @endif    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Created</td>
                                                            <td>:</td>
                                                            <td><b>{{ $playlist->created_at->format("l, d F Y") }}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Course</td>
                                                            <td>:</td>
                                                            <td><b>{{ $playlist->course->name }}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status Access</td>
                                                            <td>:</td>
                                                            <td><b>{{ $playlist->status_access }}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Publish Status</td>
                                                            <td>:</td>
                                                            <td><b>{{ $playlist->publish_status }}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Level</td>
                                                            <td>:</td>
                                                            <td><b>{{ $playlist->level }}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Certificate</td>
                                                            <td>:</td>
                                                            <td><b>{{ $playlist->certificate }}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tags</td>
                                                            <td>:</td>
                                                            <td>
                                                                @foreach ($playlist->coursetags as $tag)
                                                                    <b>{{ $tag->name }}</b>,
                                                                @endforeach    
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-12 p-3">
                                                    <h4>Description</h4>
                                                    {!! nl2br($playlist->description) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection