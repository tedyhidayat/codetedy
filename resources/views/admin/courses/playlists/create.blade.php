@extends('layouts.admin.app', ['title' => 'Create New Playlist'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('playlist-store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('admin.courses.playlists.form')
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection