@extends('layouts.admin.app', ['title' => 'Educations'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('education-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Add New Education</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50">NO.</th>
                                    <th>Education Name</th>
                                    <th>Graduation Year</th>
                                    <th>Ijazah Number</th>
                                    <th>Score</th>
                                    <th>Notes</th>
                                    <th width="130">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @forelse ($educations as $education)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $education->education_name }}</td>
                                        <td>{{ $education->graduation }}</td>
                                        <td>{{ $education->ijazah_number }}</td>
                                        <td>{{ $education->score }}</td>
                                        <td>{{ $education->notes }}</td>
                                        <td>
                                            <a href="{{ route('education-edit', $education->id) }}" class="btn btn-info btn-xs">Edit</a>
                                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $education->id }}">
                                                Delete
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="modal{{ $education->id }}" tabindex="-1" role="dialog"
                                                aria-labelledby="modalTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{ $education->education_name }}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary btn-xs"
                                                                data-dismiss="modal">Cancle</button>
                                                            <form action="{{ route('education-destroy', $education->id) }}" method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    
                                @endforelse
                            </tbody>
                            {{-- <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                            </tfoot> --}}
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection