<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Education Name</label>
            <input type="text" name="education_name" class="form-control @error('education_name') is-invalid @enderror" value="{{ old('education_name') ?? $education->education_name }}">
            @error('education_name')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Graduation Year</label>
            <input type="number" name="graduation" class="form-control @error('graduation') is-invalid @enderror" value="{{ old('graduation') ?? $education->graduation }}">
            @error('graduation')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Ijazah Number</label>
            <input type="text" name="ijazah_number" class="form-control @error('ijazah_number') is-invalid @enderror" value="{{ old('ijazah_number') ?? $education->ijazah_number }}">
            @error('ijazah_number')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Score</label>
            <input type="text" name="score" class="form-control @error('score') is-invalid @enderror" value="{{ old('score') ?? $education->score }}">
            @error('score')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Notes</label>
            <textarea rows="4" name="notes" class="form-control">{{ old('notes') ?? $education->notes }}</textarea>
            @error('notes')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>