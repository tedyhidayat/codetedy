<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Skill</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ?? $skill->name }}">
            @error('name')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Level</label>
            <select name="level" class="form-control">
                <option disabled @if(!$skill->level) selected @endif>Choose Level</option>
                @if($skill->level == 'beginer')
                <option value="beginer">Beginer</option>
                @endif
                @if($skill->level == 'intermidate')
                <option value="intermidate">Intermidate</option>
                @endif
                @if($skill->level == 'expert')
                <option value="expert">Expert</option>
                @endif
                <option value="beginer">Beginer</option>
                <option value="intermidate">Intermidate</option>
                <option value="expert">Expert</option>
            </select>

            @error('thumbnail')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Thumbnail</label>
            <input type="file" name="thumbnail" class="form-control @error('thumbnail') is-invalid @enderror" value="{{ old('thumbnail') ?? $skill->thumbnail }}">
            @error('thumbnail')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Notes</label>
            <textarea rows="7" name="notes" class="form-control textarea">{{ old('notes') ?? $skill->notes }}</textarea>
            @error('notes')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>