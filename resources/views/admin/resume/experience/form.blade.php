<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            <label>Company Name</label>
            <input type="text" name="company_name" class="form-control @error('company_name') is-invalid @enderror" value="{{ old('education_name') ?? $experience->company_name }}">
            @error('company_name')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Position</label>
            <input type="text" name="position" class="form-control @error('position') is-invalid @enderror" value="{{ old('position') ?? $experience->position }}">
            @error('position')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Since</label>
            <input type="text" name="since" class="form-control @error('since') is-invalid @enderror" value="{{ old('since') ?? $experience->since }}">
            @error('since')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label>Job Desc</label>
            <textarea name="jobdesc" class="form-control textarea">{{ old('jobdesc') ?? $experience->jobdesc }}</textarea>
            @error('jobdesc')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>