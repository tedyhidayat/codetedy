@extends('layouts.admin.app', ['title' => 'Experience'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('experience-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Add New Experience</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="50">NO.</th>
                                        <th>Company Name</th>
                                        <th>Position</th>
                                        <th>Since</th>
                                        <th>Job Desc</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @forelse ($experiences as $experience)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $experience->company_name }}</td>
                                            <td>{{ $experience->position }}</td>
                                            <td>{{ $experience->since }}</td>
                                            <td>{{ $experience->jobdesc }}</td>
                                            <td>
                                                <a href="{{ route('experience-edit', $experience->id) }}" class="btn btn-info btn-xs">Edit</a>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $experience->id }}">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                    @empty
                                        
                                    @endforelse
                                    
                                </tbody>
                            </table>
                        </div>
                        @foreach ($experiences as $experience)
                        <div class="modal fade" id="modal{{ $experience->id }}" tabindex="-1"
                            role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {{ $experience->company_name }}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Cancle</button>
                                        <form action="{{ route('experience-destroy', $experience->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>

<!-- /.content -->
@endsection