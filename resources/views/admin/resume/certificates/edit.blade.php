@extends('layouts.admin.app', ['title' => 'Edit Post Certificate'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('certificate-update', $certificate->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('patch')
                                @include('admin.resume.certificates.form')
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection