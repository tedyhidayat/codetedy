@extends('layouts.admin.app', ['title' => 'Add New Certificate'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('certificate-store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('admin.resume.certificates.form')
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection