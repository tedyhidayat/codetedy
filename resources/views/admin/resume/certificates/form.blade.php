<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Certificate Name</label>
            <input type="text" name="certificate_name" class="form-control @error('certificate_name') is-invalid @enderror" value="{{ old('certificate_name') ?? $certificate->certificate_name }}">
            @error('certificate_name')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Certificate Number</label>
            <input type="text" name="certificate_number" class="form-control @error('certificate_number') is-invalid @enderror" value="{{ old('certificate_number') ?? $certificate->certificate_number }}">
            @error('certificate_number')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Organizer</label>
            <input type="text" name="organizer" class="form-control @error('organizer') is-invalid @enderror" value="{{ old('organizer') ?? $certificate->organizer }}">
            @error('organizer')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Place</label>
            <input type="text" name="place" class="form-control @error('place') is-invalid @enderror" value="{{ old('place') ?? $certificate->place }}">
            @error('place')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Upload Certificate</label>
            <input type="file" name="file" class="form-control @error('file') is-invalid @enderror" value="{{ old('file') ?? $certificate->file }}">
            @error('file')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Notes</label>
            <textarea rows="7" name="notes" class="form-control">{{ old('notes') ?? $certificate->notes }}</textarea>
            @error('notes')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<button type="submit" class="btn btn-primary">{{ $submit ?? 'Update' }}</button>