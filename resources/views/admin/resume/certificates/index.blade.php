@extends('layouts.admin.app', ['title' => 'Certificates'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('certificate-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Add New Certificate</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="50">NO.</th>
                                        <th>Certificate Name</th>
                                        <th>Certificate Number</th>
                                        <th>Organizer</th>
                                        <th>Place</th>
                                        <th>File</th>
                                        <th>Notes</th>
                                        <th width="130">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @forelse ($certificates as $certificate)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $certificate->certificate_name }}</td>
                                            <td>{{ $certificate->certificate_number }}</td>
                                            <td>{{ $certificate->organizer }}</td>
                                            <td>{{ $certificate->place }}</td>
                                            <td><a target="_blank" href="{{ asset("storage/".$certificate->file) }}">{{ $certificate->file }}</a></td>
                                            <td>{{ $certificate->notes }}</td>
                                            <td>
                                                <a href="{{ route('certificate-edit', $certificate->id) }}" class="btn btn-info btn-xs">Edit</a>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $certificate->id }}">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                    @empty
                                        
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- Modal -->
                        @foreach ($certificates as $certificate)
                        <div class="modal fade" id="modal{{ $certificate->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="modalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {{ $certificate->certificate_name }}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-xs"
                                            data-dismiss="modal">Cancle</button>
                                        <form action="{{ route('certificate-destroy', $certificate->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection