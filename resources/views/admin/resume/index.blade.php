@extends('layouts.admin.app', ['title' => 'Skils'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('skill-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Add New skill</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="50">NO.</th>
                                        <th>Icon</th>
                                        <th>Skill</th>
                                        <th>Level</th>
                                        <th>Notes</th>
                                        <th width="130">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @forelse ($skills as $skill)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td><img class="img-thumbnail" width="40" target="_blank" src="{{ asset("storage/".$skill->thumbnail) }}" /></td>
                                            <td>{{ $skill->name }}</td>
                                            <td>{{ $skill->level }}</td>
                                            <td>{!! $skill->notes !!}</td>
                                            <td>
                                                <a href="{{ route('skill-edit', $skill->id) }}" class="btn btn-info btn-xs">Edit</a>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $skill->id }}">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                    @empty
                                        
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- Modal -->
                        @foreach ($skills as $skill)
                        <div class="modal fade" id="modal{{ $skill->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="modalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {{ $skill->name }}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-xs"
                                            data-dismiss="modal">Cancle</button>
                                        <form action="{{ route('skill-destroy', $skill->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection