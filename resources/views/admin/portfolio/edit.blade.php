@extends('layouts.admin.app', ['title' => 'Edit Portfolio'])

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                Images
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @foreach ($portfolio->image as $image)
                                    <div class="col-6 col-md-2">
                                        <div class="card">
                                            <a href="{{ asset('storage/' . $image->img_name) }}" data-toggle="lightbox" data-title="{{ $image->img_name }}" data-gallery="gallery">
                                                <img src="{{ asset('storage/' . $image->img_name) }}" width="150" class="img-card-top img-fluid" style="width: 100%; height:150px; object-fit:cover; object-position:center;">
                                            </a>
                                            <div class="card-body p-1">
                                                <form action="{{ route('portfolio-img-delete', $image->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn text-danger"><i class="fas fa-trash"></i> Delete</button>
                                                </form>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                @endforeach
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-app btn-warning" data-toggle="modal" data-target="#modal">
                                        <i class="fas fa-plus"></i>
                                        <span>Add Image</span>
                                    </button>
                                </div>
                            </div>
                            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Add New Image</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('portfolio-img-add') }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <input type="hidden" name="name" value="{{ $portfolio->name }}">
                                                    <input type="hidden" name="portfolio_id" value="{{ $portfolio->id }}">
                                                    <input type="file" name="thumbnail[]" class="form-control dropify"
                                                    data-allowed-file-extensions="jpg jpeg png svg"
                                                    data-max-file-size="3M" multiple>
                                                </div>
                                                <button type="submit" class="btn btn-sm btn-success btn-block">Save Image</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Detail</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <form action="{{ route('portfolio-update', $portfolio->id) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('patch')
                                        @include('admin.portfolio.form')
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('script-after')
<!-- Ekko Lightbox -->
<script src="{{ asset('adminlte3/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
<!-- bs-custom-file-input -->
<script src="{{ asset('adminlte3/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<script type="text/javascript">
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            alwaysShowClose: true
        });
    });

    $(document).ready(function() {
        $("body").on("click", ".addImage", function() {
            addImage();
        });

        function addImage() {
            var image = '<div class="form-group"><div class="input-group control-group mb-2"><input type="file" name="thumbnail[]" class="form-control"><div class="input-group-append"><button class="btn btn-danger remove" type="button"><i class="fas fa-trash"></i></button></div></div></div>';
            $(".imageNew").append(image);
        }
        $("body").on("click", ".remove", function() {
            $(this).parent().parent().parent().remove();
        });
    });
</script>
@endsection