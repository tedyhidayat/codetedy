@extends('layouts.admin.app', ['title' => 'Add New Portfolio'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('portfolio-store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('admin.portfolio.form')
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection

@section('script-after')
<!-- bs-custom-file-input -->
<script src="{{ asset('adminlte3/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("body").on("click", ".addImage", function() {
            addImage();
        });

        function addImage() {
            var image = '<div class="form-group"><div class="input-group control-group mb-2"><input type="file" name="thumbnail[]" class="form-control"><div class="input-group-append"><button class="btn btn-danger remove" type="button"><i class="fas fa-trash"></i></button></div></div></div>';
            $(".imageNew").append(image);
        }
        $("body").on("click", ".remove", function() {
            $(this).parent().parent().parent().remove();
        });
    });
</script>
@endsection