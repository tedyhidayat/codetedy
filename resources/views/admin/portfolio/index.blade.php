@extends('layouts.admin.app', ['title' => 'Portfolio'])

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('portfolio-create') }}" class="btn btn-xs btn-success"> <i class="fas fa-plus"></i> Add New Portfolio</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="50">NO.</th>
                                        <th width="50">Thumbnail</th>
                                        <th>Portfolio Name</th>
                                        <th>Category</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th width="130">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @forelse ($portfolios as $portfolio)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>
                                                <img src="{{ asset("storage/" . $portfolio->image->first()->img_name) }}" width="50" class="img-thumbnail">
                                            </td>
                                            <td>{{ $portfolio->name }}</td>
                                            <td>{{ $portfolio->category->name }}</td>
                                            <td>{{ $portfolio->date_created->format('d F Y') }}</td>
                                            <td>
                                                @if($portfolio->is_active == 1)
                                                    <a href="{{ route('portfolio-isActive', [$portfolio->id, $portfolio->is_active]) }}" class="btn btn-success btn-xs"> ON</a>
                                                @else
                                                    <a href="{{ route('portfolio-isActive', [$portfolio->id, $portfolio->is_active]) }}" class="btn btn-danger btn-xs"> OFF</a>
                                                @endif
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalDetail{{ $portfolio->id }}">
                                                    Detail
                                                </button>
                                                <a href="{{ route('portfolio-edit', $portfolio->id) }}" class="btn btn-info btn-xs">Edit</a>
                                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal{{ $portfolio->id }}">
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                    @empty
                                        
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- Modal -->
                        @foreach ($portfolios as $portfolio)
                        <div class="modal fade" id="modal{{ $portfolio->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="modalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Are you sure ?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {{ $portfolio->name }}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-xs"
                                            data-dismiss="modal">Cancle</button>
                                        <form action="{{ route('portfolio-destroy', $portfolio->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-xs">Delete Now</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modalDetail{{ $portfolio->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="modalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">{{ $portfolio->name }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            @foreach ($portfolio->image as $img)
                                                <div class="col-md-4">
                                                    <img src="{{ asset("storage/" . $img->img_name) }}" class="img-thumbnail img-fluid" style="width: 100%; height:150px; object-fit:cover; object-position:center;">
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <table class="mt-3 table table-borderless table-striped">
                                                    <tr>
                                                        <th>Date Created</th>
                                                        <th>:</th>
                                                        <th>{{ $portfolio->date_created->format('l, d F Y') }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Client</th>
                                                        <th>:</th>
                                                        <th>{{ $portfolio->client }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Link Project</th>
                                                        <th>:</th>
                                                        <th>{{ $portfolio->link }}</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col p-2">
                                                <h5 class="mb-3">Notes</h5>
                                                {!!$portfolio->notes!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection