<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            <label>Portfolio Name</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ?? $portfolio->name }}" autofocus>
            @error('name')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Client</label>
            <input type="text" name="client" class="form-control @error('client') is-invalid @enderror" value="{{ old('client') ?? $portfolio->client }}">
            @error('client')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Category</label>
            <select name="category" class="form-control select2" style="width:100%;">
                <option {{ !$portfolio->category_id ? 'selected' : '' }} disabled>Select Category</option>
                @foreach ($categories as $category)
                    <option {{ $portfolio->category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            @error('category')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Created at</label>
            <input type="date" name="created_date" class="form-control @error('created_date') is-invalid @enderror" @if($portfolio->date_created) value="{{ old('created_date') ?? $portfolio->date_created->format('Y-m-d') }}" @else value="{{ old('created_date') ?? $portfolio->date_created }}" @endif>
            @error('created_date')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Link Portfolio</label>
            <input type="text" name="link" class="form-control @error('link') is-invalid @enderror" value="{{ old('link') ?? $portfolio->link }}">
            @error('link')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-7">
        @if(!$portfolio->id)
            <div class="form-group">
                <label>Upload Image Portfolio</label>        
                <div class="input-group control-group mb-2">
                    <input type="file" name="thumbnail[]" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-success addImage" type="button"><i class="fas fa-plus"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group imageNew"></div>
        @endif
        <div class="form-group">
            <label>Notes</label>
            <textarea rows="7" name="notes" class="form-control textarea">{{ old('notes') ?? $portfolio->notes }}</textarea>
            @error('notes')
                <div class="text-danger text small invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<button type="submit" class="btn btn-primary float-right">{{ $submit ?? 'Save Changes' }}</button>