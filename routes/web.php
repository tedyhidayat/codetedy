<?php

// use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\{Auth, Route};
use Symfony\Component\Routing\Route as RoutingRoute;


Route::get('/', function () {
    return view('welcome');
});

Route::prefix('administrator/')
    ->namespace('Admin')
    ->group(function(){

        Route::get('dashboard', 'DashboardController@index')->name('admin-dashboard');

         // portfolios
        Route::prefix('portfolio/')
        ->group(function(){
            // categories
            Route::get('categories/', 'PortfolioCategoryController@index')->name('portfolio-category');
            Route::get('categories/create', 'PortfolioCategoryController@create')->name('portfolio-category-create');
            Route::post('categories/store', 'PortfolioCategoryController@store')->name('portfolio-category-store');
            Route::get('categories/{category:id}/edit', 'PortfolioCategoryController@edit')->name('portfolio-category-edit');
            Route::patch('categories/{category:id}/update', 'PortfolioCategoryController@update')->name('portfolio-category-update');
            Route::delete('categories/{category:id}/destroy', 'PortfolioCategoryController@destroy')->name('portfolio-category-destroy');

            // portffolio
            Route::get('', 'PortfolioController@index')->name('portfolio-show');
            Route::get('create', 'PortfolioController@create')->name('portfolio-create');
            Route::post('store', 'PortfolioController@store')->name('portfolio-store');
            Route::get('{portfolio:id}/edit', 'PortfolioController@edit')->name('portfolio-edit');
            Route::patch('{portfolio:id}/update', 'PortfolioController@update')->name('portfolio-update');
            Route::delete('{portfolio:id}/destroy', 'PortfolioController@destroy')->name('portfolio-destroy');
            Route::get('{id}/{status}/is-active', 'PortfolioController@isActive')->name('portfolio-isActive');
            Route::delete('{id}/img-delete', 'PortfolioController@imgDelete')->name('portfolio-img-delete');
            Route::post('img-add', 'PortfolioController@imgAdd')->name('portfolio-img-add');
        });

        // Course
        Route::prefix('courses/')
        ->group(function(){
            // course
            Route::get('', 'CourseController@index')->name('course');
            Route::get('create', 'CourseController@create')->name('course-create');
            Route::post('store', 'CourseController@store')->name('course-store');
            Route::get('{course:id}/edit', 'CourseController@edit')->name('course-edit');
            Route::patch('{course:id}/update', 'CourseController@update')->name('course-update');
            Route::delete('{course:id}/destroy', 'CourseController@destroy')->name('course-destroy');
            
            // playlist
            Route::get('playlists/', 'PlaylistController@index')->name('playlist');
            Route::get('playlists/create', 'PlaylistController@create')->name('playlist-create');
            Route::post('playlists/store', 'PlaylistController@store')->name('playlist-store');
            Route::get('playlists/{playlist:id}/edit', 'PlaylistController@edit')->name('playlist-edit');
            Route::patch('playlists/{playlist:id}/update', 'PlaylistController@update')->name('playlist-update');
            Route::delete('playlists/{playlist:id}/destroy', 'PlaylistController@destroy')->name('playlist-destroy');
            Route::get('playlists/{id}/{status}/is-active', 'PlaylistController@isActive')->name('playlist-isActive');
            
            // Videos
            Route::get('playlists/videos/', 'VideoController@index')->name('video');
            Route::get('playlists/videos/create', 'VideoController@create')->name('video-create');
            Route::post('playlists/videos/store', 'VideoController@store')->name('video-store');
            Route::get('playlists/videos/{video:id}/edit', 'VideoController@edit')->name('video-edit');
            Route::patch('playlists/videos/{video:id}/update', 'VideoController@update')->name('video-update');
            Route::delete('playlists/videos/{video:id}/destroy', 'VideoController@destroy')->name('video-destroy');
            Route::get('playlists/videos/{id}/{status}/is-active', 'VideoController@isActive')->name('video-isActive');
            
            // categories
            Route::get('categories/', 'CourseCategoryController@index')->name('course-category');
            Route::get('categories/create', 'CourseCategoryController@create')->name('course-category-create');
            Route::post('categories/store', 'CourseCategoryController@store')->name('course-category-store');
            Route::get('categories/{category:id}/edit', 'CourseCategoryController@edit')->name('course-category-edit');
            Route::patch('categories/{category:id}/update', 'CourseCategoryController@update')->name('course-category-update');
            Route::delete('categories/{category:id}/destroy', 'CourseCategoryController@destroy')->name('course-category-destroy');
            
            // tags
            Route::get('tags/', 'CourseTagController@index')->name('course-tag');
            Route::get('tags/create', 'CourseTagController@create')->name('course-tag-create');
            Route::post('tags/store', 'CourseTagController@store')->name('course-tag-store');
            Route::get('tags/{tag:id}/edit', 'CourseTagController@edit')->name('course-tag-edit');
            Route::patch('tags/{tag:id}/update', 'CourseTagController@update')->name('course-tag-update');
            Route::delete('tags/{tag:id}/destroy', 'CourseTagController@destroy')->name('course-tag-destroy');
        });
        
        // posts
        Route::prefix('posts/')
        ->group(function(){
            // post
            Route::get('', 'PostController@index')->name('post-show');
            Route::get('create', 'PostController@create')->name('post-create');
            Route::post('store', 'PostController@store')->name('post-store');
            Route::get('{post:id}/edit', 'PostController@edit')->name('post-edit');
            Route::patch('{post:id}/update', 'PostController@update')->name('post-update');
            Route::delete('{post:id}/destroy', 'PostController@destroy')->name('post-destroy');
            Route::get('{id}/{status}/is-active', 'PostController@isActive')->name('post-isActive');
            
            // categpries
            Route::get('categories/', 'PostCategoryController@index')->name('post-category');
            Route::get('categories/create', 'PostCategoryController@create')->name('post-category-create');
            Route::post('categories/store', 'PostCategoryController@store')->name('post-category-store');
            Route::get('categories/{category:id}/edit', 'PostCategoryController@edit')->name('post-category-edit');
            Route::patch('categories/{category:id}/update', 'PostCategoryController@update')->name('post-category-update');
            Route::delete('categories/{category:id}/destroy', 'PostCategoryController@destroy')->name('post-category-destroy');
            
            // tags
            Route::get('tags/', 'PostTagController@index')->name('post-tag');
            Route::get('tags/create', 'PostTagController@create')->name('post-tag-create');
            Route::post('tags/store', 'PostTagController@store')->name('post-tag-store');
            Route::get('tags/{tag:id}/edit', 'PostTagController@edit')->name('post-tag-edit');
            Route::patch('tags/{tag:id}/update', 'PostTagController@update')->name('post-tag-update');
            Route::delete('tags/{tag:id}/destroy', 'PostTagController@destroy')->name('post-tag-destroy');
        });

        Route::prefix('resume/')
        ->group(function() {
            // educations
            Route::get('education/', 'EducationController@index')->name('education-show');
            Route::get('education/create', 'EducationController@create')->name('education-create');
            Route::post('education/store', 'EducationController@store')->name('education-store');
            Route::get('education/{education:id}/edit', 'EducationController@edit')->name('education-edit');
            Route::patch('education/{education:id}/update', 'EducationController@update')->name('education-update');
            Route::delete('education/{education:id}/destroy', 'EducationController@destroy')->name('education-destroy');
            
            // work experience
            Route::get('experience/', 'ExperienceController@index')->name('experience-show');
            Route::get('experience/create', 'ExperienceController@create')->name('experience-create');
            Route::post('experience/store', 'ExperienceController@store')->name('experience-store');
            Route::get('experience/{experience:id}/edit', 'ExperienceController@edit')->name('experience-edit');
            Route::patch('experience/{experience:id}/update', 'ExperienceController@update')->name('experience-update');
            Route::delete('experience/{experience:id}/destroy', 'ExperienceController@destroy')->name('experience-destroy');
            
            // certificates
            Route::get('certificate/', 'CertificateController@index')->name('certificate-show');
            Route::get('certificate/create', 'CertificateController@create')->name('certificate-create');
            Route::post('certificate/store', 'CertificateController@store')->name('certificate-store');
            Route::get('certificate/{certificate:id}/edit', 'CertificateController@edit')->name('certificate-edit');
            Route::patch('certificate/{certificate:id}/update', 'CertificateController@update')->name('certificate-update');
            Route::delete('certificate/{certificate:id}/destroy', 'CertificateController@destroy')->name('certificate-destroy');

            // certificates
            Route::get('skill/', 'SkillController@index')->name('skill-show');
            Route::get('skill/create', 'SkillController@create')->name('skill-create');
            Route::post('skill/store', 'SkillController@store')->name('skill-store');
            Route::get('skill/{skill:id}/edit', 'SkillController@edit')->name('skill-edit');
            Route::patch('skill/{skill:id}/update', 'SkillController@update')->name('skill-update');
            Route::delete('skill/{skill:id}/destroy', 'SkillController@destroy')->name('skill-destroy');
        });

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
