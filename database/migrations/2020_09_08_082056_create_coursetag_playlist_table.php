<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursetagPlaylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coursetag_playlist', function (Blueprint $table) {
            $table->foreignId('coursetag_id')->constrained('coursetags');
            $table->foreignId('playlist_id')->constrained('playlists');
            $table->primary(['coursetag_id', 'playlist_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coursetag_playlist');
    }
}
